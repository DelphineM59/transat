<?php ob_start(); 
session_start();
?>
<?php
$titre = 'Transat Jacques Vabre 2007 : liste des bateaux'; ?>
<?php 
// récupération des variables passées en get
$idnomClasse = isset($_GET["idClasse"]);

if (($idnomClasse)) {
    $idClasse = intval(htmlspecialchars($_GET["idClasse"]));
}

// access à la base de données
require "admin/bdd/bddconfig.php";
try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $PDObateaux = $objBdd->prepare("SELECT * from bateau where");

    $PDObateaux = $objBdd->prepare("select * from bateau where idClasse= :idClasse ORDER by classementFinal ASC");
    $PDObateaux->execute(array(':idClasse' => $idClasse));
    $PDObateaux->execute();
    
    $PDOclassBateau = $objBdd->prepare("SELECT * from classebateau where idClasse= :idClasse");
    $PDOclassBateau->execute(array(':idClasse' => $idClasse));
    $PDOclassBateau->execute();
    $row_classBateau = $PDOclassBateau->fetch();


// lecture de la table classement
?>
<article>
    <h1>Classement des bateaux</h1>
    <table>
    <tr>
    <td></td>
    <td><?= $row_classBateau['nomClasse'];?></td>
    </tr>
    <?php while($unBateau = $PDObateaux->fetch()) { ?>
        <tr>
            <td>
            <?php 
             if ($unBateau['classementFinal']==9999){
                 echo "AB";
             }else{
            echo $unBateau['classementFinal'];
             } ?>
            </td>
            <td><a href="detailbateaux.php?idBateau=<?= $unBateau['idBateau']; ?>"><?= $unBateau['nomBateau']; ?></a>
            </td>
        </tr>
    <?php } ?>
    </table>
</article>
<?php
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
    }
$contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>