<?php ob_start();
session_start();

?>
<?php
$titre = 'Transat Jacques Vabre 2007 : les classes de bateaux'; ?>
<?php 
// access à la base de données
require "admin/bdd/bddconfig.php";
try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $PDOclassBateau = $objBdd->prepare("SELECT * from classebateau order by typeCoque");
    $PDOclassBateau->execute();

?>
<article>
    <h1>Classement des bateaux</h1>
    <table>
    <tr>
    <td></td>
    <td>Classes</td>
    </tr>
    <?php
    while($unTypeBateau = $PDOclassBateau->fetch()) { ?>
        <tr>
            <td><?= $unTypeBateau['typeCoque']; ?>
            </td>
            <td><a href="listebateaux.php?idClasse=<?= $unTypeBateau['idClasse']; ?>"><?= $unTypeBateau['nomClasse']; ?></a>
            </td>
        </tr>
    <?php } ?>
    </table>
</article>
<?php 
} catch (Exception $prmE) {
die('Erreur : ' . $prmE->getMessage());
}
$contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>