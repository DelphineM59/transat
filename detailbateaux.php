<?php ob_start();
session_start();
?>
<?php
$titre = 'Transat Jacques Vabre 2007 : détail des bateaux'; ?>
<?php 
// récupération des variables passées en get
$idBateauok = isset($_GET["idBateau"]);

if (isset($idBateauok)) {
    $idBateau = intval(htmlspecialchars($_GET["idBateau"]));
}

// access à la base de données
require "admin/bdd/bddconfig.php";
try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $PDOlesbateaux = $objBdd->prepare("SELECT * from bateau where idBateau= :idBateau");
    $PDOlesbateaux->execute(array(':idBateau' => $idBateau));
    $PDOlesbateaux->execute();
    $row_lesbateaux = $PDOlesbateaux->fetch();

    $PDOlesskippers = $objBdd->prepare("SELECT * from skipper where idBateau= :idBateau");
    $PDOlesskippers->execute(array(':idBateau' => $idBateau));
    $PDOlesskippers->execute();
    $row_lesskppers = $PDOlesskippers->fetch();

?>
<article>
    <h1>Le bateau et son skipper</h1>
<div id="div-image">
    <div>
    <?php
    if (($row_lesbateaux != false) && ($row_lesskppers != false)){
    ?>
        <img src="images/bateaux/<?= $row_lesbateaux['photo']; ?>" class="div-image" alt="le bateau">
        <p><?= $row_lesbateaux['nomBateau']; ?></p>

    </div>
    <div>
    <img src="images/skippers/<?= $row_lesskppers['photo']; ?>" class="div-image" alt="le skipper">
    <p><?= $row_lesskppers['nomSkipper']; ?></p>
    <?php } else {
        echo "erreur, pas de bateaux";
    } ?>
    </div>
</div>
</article>
<?php 
} catch (Exception $prmE) {
die('Erreur : ' . $prmE->getMessage());
}
$contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>