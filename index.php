<?php ob_start();
session_start();
?>
<?php
$titre = 'Transat Jacques Vabre 2007'; ?>

    <article>                
         <h1>Michel DESJOYEAUX vainqueur de la transat Jacques-Vabre 2007</h1>
         <p>Le départ a été donné le 3 novembre 2007. Le parcours relie Le Havre à Salvador de bahia (Brésil).</p>
         <p>Michel DESJOYEAUX, avec Emmanuel Le Borgne, sur Foncia (Classe Monocoque 60 IMOCA) ont gagné !</p>
         <p><img src="images/vainqueur.jpg" /></p>
    </article>
<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>