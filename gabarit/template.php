<?php
require "admin/bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);


try {
    $objBdd = new PDO(
        "mysql:host=$bddserver;
        dbname=$bddname;
        charset=utf8",
        $bddlogin,
        $bddpass
    );

    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php $titre; ?></title>
        <link rel="stylesheet" href="css/transat.css" />
    </head>

    <body>
        <div id="conteneur">
            <header>
                <div class='login'>

                <?php 

                // verifie si le membre est authentifie
                    if (isset($_SESSION['logged_in']['login']) == TRUE) {
                    //l'internaute est authentifié
                    echo $_SESSION['logged_in']['login']."</br>";?>
                    
                    <a href="logout.php">Se déconnecter</a>
                    <?php }
                    //affichage "Se déconnecter"(logout.php), "Prif", "Paramètres", etc ...
                    if (isset($_SESSION['logged_in']['login']) == FALSE) {
                    ?>
                        <form method="POST" action="login_action.php">
                        <fieldset>
                            <legend>Connectez-vous</legend>
                            <input type="text" name="login" value="" placeholder="Login" required>
                            <input type="text" name="password" value="" placeholder="Password" required>
                            <input type="submit" value="Valider">
                        </fieldset>
                    </form>   
                    <?php }?>
                </div>
            </header>

            <nav>
                <ul>
                    <li><a href="index.php" class="navitem">Accueil</a></li>
                    <li><a href="classements.php" class="navitem">Classements</a></li>
                    <?php if (isset($_SESSION['logged_in']['login']) == TRUE) {?>
                    <li><a href="ajoutclasse.php" class="navitemprivate">Ajout Classe</a></li>
                    <?php } ?>
                </ul>
            </nav>
            <section>
            <?php echo $contenu; ?>
            </section>
            <footer>
                <p>Copyright Moi - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>
