<?php ob_start();
session_start();
?>
<?php
$titre = 'Transat Jacques Vabre 2007 : ajout classe';
    //Connexion à la base et insertion du nouvel utilisateur
    require "admin/bdd/bddconfig.php";
    // on verifie que l'utilisateur est logué

    // recuperation des variables en post
    $nomClasseok = isset($_POST["nomClasse"]);
    $typeCoqueok = isset($_POST["typeCoque"]);
    $taillecoqueok = isset($_POST["taillecoque"]);

    if ((isset($_SESSION['logged_in']['login']) == TRUE) && (isset($nomClasseok)) && (isset($typeCoqueok)) && (isset($taillecoqueok))) {
        $nomClasse = strval(htmlspecialchars($_POST["nomClasse"]));
        $typeCoque = strval(htmlspecialchars($_POST["typeCoque"]));
        $taillecoque = intval(htmlspecialchars($_POST["taillecoque"]));
    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $PDOinsertClasse = $objBdd->prepare("INSERT INTO classebateau (nomClasse,typeCoque, taillecoque) VALUES (:nomClasse, :typeCoque, :taillecoque)");
        
        $PDOinsertClasse->bindParam(':nomClasse', $nomClasse, PDO::PARAM_STR);
        $PDOinsertClasse->bindParam(':typeCoque', $typeCoque, PDO::PARAM_STR);
        $PDOinsertClasse->bindParam(':taillecoque', $taillecoque, PDO::PARAM_INT);
        $PDOinsertClasse->execute();
        //récupérer la valeur de l'ID du nouveau champ créé
        ?>
    <article>      
    Vous venez d'insérer une classe avez succés.
    </article>
        <?php
        // echo $lastId = $objBdd->lastInsertId();
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }

?>


<?php
} else {
    echo "Vous devez être loggué pour accéder à cette page";
}
$contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>