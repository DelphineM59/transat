<?php ob_start(); 
session_start();
?>

<?php
$titre = 'Transat Jacques Vabre 2007 : les classes de bateaux'; ?>
<?php 
// access à la base de données
require "admin/bdd/bddconfig.php";
try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $PDOclassBateau = $objBdd->prepare("SELECT * from classebateau order by typeCoque");
    $PDOclassBateau->execute();

?>
<article>
    <h1>Classement des bateaux</h1>
    <table>
    <tr>
    <td></td>
    <td>Classes en cours</td>
    </tr>
    <?php
    while($unTypeBateau = $PDOclassBateau->fetch()) { ?>
        <tr>
            <td><?= $unTypeBateau['typeCoque']; ?>
            </td>
            <td><?= $unTypeBateau['nomClasse']; ?>
            </td>
        </tr>
    <?php } ?>
    </table>
</article>
<p></p><p></p>
<article>
<form method="POST" action="ajoutclasse_action.php">
    <Label for="nomClasse">Nom de la classe</Label>
    <input type="text" name="nomClasse" placeholder="" required></br>
    <Label for="typeCoque">Type de coque : </Label>
    <select name="typeCoque" id="">
    <option value="Monocoque">Monocoque</option>
    <option value="Multicoque">Multicoque</option>
    </select></br>
    <Label for="taillecoque">Taille de la coque</Label>
    <input type="taillecoque" name="taillecoque" placeholder="" required></br>
    <input type="submit" value="Ajouter classe">
</form>

</article>
<p></p><p></p><p></p><p></p><p></p><p></p>
<?php 
} catch (Exception $prmE) {
die('Erreur : ' . $prmE->getMessage());
}
$contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>